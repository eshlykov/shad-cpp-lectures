#include <gmock/gmock.h>

#include <numeric>

int Add(int a, int b) {
    if (a == 9999) {
        return b;
    }
    return a + b + 1;
}

TEST(Sum, SmallNumbers) {
    int x = 1;
    int y = 2, z = 3;
    EXPECT_TRUE(Add(x, y) == z); // <- DO NOT DO THIS

    EXPECT_EQ(Add(1, 2), 3);
}

TEST(Sum, BigRange) {
    for (int i = 0; i < 10000; i++) {
        // <- DO NOT DO THIS
        EXPECT_EQ(Add(100000 - i, 100000), 100000);


        // Provide context
        EXPECT_EQ(Add(100000 - i, 100000), 100000)
            << "i = " << i;
    }
}

TEST(Sum, Throws) {
    try {
        Add(std::numeric_limits<int>::max(), std::numeric_limits<int>::max());
        EXPECT_TRUE(false); // <- DO NOT DO THIS
    } catch (...) {

    }

    const auto IntMax = std::numeric_limits<int>::max();
    EXPECT_THROW(Add(IntMax, IntMax), std::overflow_error);
}

struct Data {
    int Foo;
    int Bar;
};

Data* Lookup(const std::string& key) {
    return nullptr;
}

TEST(Lookup, AvoidingSegfault) {
    auto data = Lookup("foo_bar");

    // DO NOT DO THIS
    EXPECT_TRUE(data);
    EXPECT_EQ(data->Foo, 10);
    EXPECT_EQ(data->Bar, 20);

    ASSERT_TRUE(data); // Stops the test
    EXPECT_EQ(data->Foo, 10); // Continues the test
    EXPECT_EQ(data->Bar, 10); // Continues the test
}

struct Doer {};

struct Checker {
    Checker(Doer* doer) {}
};

struct Worker {
    Worker(Doer* doer, Checker* checker) {}
};

struct TheThingYouAreTryingToTest {
    TheThingYouAreTryingToTest(
        Doer* doer,
        Worker* worker,
        Checker* checker) {}

    int Foo() { return 0; }
    int Bar() { return 1; }
};

TEST(TestWithoutFixture, TestingFoo) {
    Doer doer;
    Checker checker(&doer);
    Worker worker(&doer, &checker);
    TheThingYouAreTryingToTest thing(&doer, &worker, &checker);

    EXPECT_EQ(0, thing.Foo());
}

TEST(TestWithoutFixture, TestingBar) {
    // ???
}

struct TestingWithFixture : public ::testing::Test {
    Doer testDoer;
    Checker testChecker;
    Worker testWorker;
    TheThingYouAreTryingToTest thing;

    TestingWithFixture()
        : testDoer()
        , testChecker(&testDoer)
        , testWorker(&testDoer, &testChecker)
        , thing(&testDoer, &testWorker, &testChecker)
    { }
};

TEST_F(TestingWithFixture, Foo) {
    EXPECT_EQ(0, thing.Foo());
}

TEST_F(TestingWithFixture, Bar) {
    EXPECT_EQ(1, thing.Bar());
}
