#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <chrono>
#include <functional>
#include <thread>
#include <future>

struct Request {};
struct Response {};

enum class EStatus {
    Running,
    Failed,
    Finished,
};

typedef std::string OperationId;

class BatchJobCluster {
public:
    OperationId StartOperation(const std::string& cmd);

    EStatus PollOperation(const OperationId& id);
};

void Wait(
    std::chrono::system_clock::duration timeout,
    std::function<bool()> cb) {

    for (int i = 0; i <= 10; i++) {
        auto status = cb();
        if (status) {
            break;
        }

        std::this_thread::sleep_for(
            std::chrono::milliseconds(1 + i * i));
        if (i == 10) {
            FAIL();
        }
    }
}

TEST(Future, Test) {
    std::promise<int> p;
    auto f = p.get_future();

    p.set_value(1);
    EXPECT_EQ(f.get(), 1);

    std::promise<int> p1;
    auto f1 = p.get_future();
    p1.set_exception(
        std::make_exception_ptr<std::runtime_error>(
            std::string("fo")));
    EXPECT_THROW(f1.get(), std::runtime_error);
}

TEST(Cluster, Test) {
    BatchJobCluster c;
    auto id = c.StartOperation("sudo rm -rf");

    Wait(std::chrono::seconds(10), [&] () -> bool {
        auto status = c.PollOperation(id);

        if (status != EStatus::Running) {
            EXPECT_EQ(status, EStatus::Finished);
            return true;
        }

        return false;
    });
}